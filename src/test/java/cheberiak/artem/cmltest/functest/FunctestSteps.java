package cheberiak.artem.cmltest.functest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FunctestSteps {
    @Given("the geoIP server is located on (.*)")
    public void serverIsLocatedAt(String serverURL) {
        World.setServerURL(URI.create(serverURL));
    }

    @When("I make a GET request to the server for (.+) IP")
    public void getForIP(String ip) {
        Client client = ClientBuilder.newClient();
        WebTarget ipTarget = client.target(World.getServerURL()).path(ip);

        long startRequestTime = System.currentTimeMillis();
        Response response = ipTarget.request().get();
        long endRequestTime = System.currentTimeMillis();

        World.setResponseTime(endRequestTime - startRequestTime);
        World.setResponseStatus(response.getStatus());
        World.setResponseString(ipTarget.request().get().readEntity(String.class));
    }

    @Then("I get a (\\d+) response without timeout")
    public void getAResponseWithCode(int expectedStatus) {
        World.evaluateResponseCode(expectedStatus);
    }

    @Then("I get a (\\d+) response within (\\d+) milliseconds")
    public void getAResponseWithCodeWithinMilliseconds(int expectedStatus, int expectedResponseTime) {
        World.evaluateResponseCode(expectedStatus);
        assertTrue("Response took too long!", expectedResponseTime > World.getResponseTime());
    }

    @Then("response contains text:")
    public void stringResponseContains(String expectedText) {
        String responseString = World.getResponseString();
        assertTrue("Received payload did not contain the expected text!\nPayload: " + responseString,
                   responseString.contains(expectedText));
    }

    @Then("response contains a JSON payload")
    public void parseResponseStringAsJson() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> responseMap = mapper.readValue(World.getResponseString(),
                                                           new TypeReference<HashMap<String, String>>() {});
        World.setResponseJsonMap(responseMap);
    }

    @Then("response JSON contains field \"(.+)\" with value \"(.*)\"")
    public void checkResponseJsonFieldValue(String fieldName, String expectedValue) {
        assertEquals("Response status is not the expected one!", expectedValue, World.getResponseMap().get(fieldName));
    }
}
