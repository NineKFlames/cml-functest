package cheberiak.artem.cmltest.functest;

import cucumber.api.java.Before;

import java.net.URI;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class World {
    private static URI serverURL;
    private static Map<String, String> responseMap;
    private static String responseString;
    private static int responseStatus;
    private static long responseTime;

    @Before
    public static void cleanUp() {
        serverURL = null;
        responseMap = null;
    }

    static URI getServerURL() {
        return serverURL;
    }

    static void setServerURL(URI serverURL) {
        World.serverURL = serverURL;
    }

    static Map<String, String> getResponseMap() {
        return responseMap;
    }

    static void setResponseJsonMap(Map<String, String> response) {
        World.responseMap = response;
    }

    static String getResponseString() {
        return responseString;
    }

    static void setResponseString(String response) {
        World.responseString = response;
    }

    static void setResponseStatus(int responseStatus) {
        World.responseStatus = responseStatus;
    }

    static long getResponseTime() {
        return responseTime;
    }

    static void setResponseTime(long responseTime) {
        World.responseTime = responseTime;
    }

    static void evaluateResponseCode(int expectedStatus) {
        assertEquals("Response status is not the expected one!", expectedStatus, World.responseStatus);
    }
}
