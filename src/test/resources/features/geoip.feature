Feature: Geo IP service
  Service that provides geolocation based on IP

  Background:
    Given the geoIP server is located on http://localhost:8080/geoip/

  Scenario: Get location for a valid canonical IP address
    When I make a GET request to the server for 1.1.1.1 IP
    Then I get a 200 response without timeout
    And response contains a JSON payload
    And response JSON contains field "canonicalIPv4Representation" with value "1.1.1.1"
    And response JSON contains field "cityName" with value "Brisbane"
    And response JSON contains field "countryCode" with value "AU"
    And response JSON contains field "countryName" with value "Australia"
    And response JSON contains field "ipv4" with value "16843009"
    And response JSON contains field "latitude" with value "-27.46794"
    And response JSON contains field "longitude" with value "153.02809"
    And response JSON contains field "regionName" with value "Queensland"

  Scenario: Get location for a valid canonical IP address within time limit
    When I make a GET request to the server for 1.1.1.1 IP
    Then I get a 200 response within 100 milliseconds

  Scenario: Get location for a non-IP string
    When I make a GET request to the server for NO_THIS_IS_NOT_AN_IP IP
    Then I get a 400 response without timeout
    And response contains text:
    """
    Dude, is that in IP? I don't think so.
    """
