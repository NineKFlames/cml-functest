# Functional test for By-IP-finder
This one is completely separated specifically to also include some network fluctuations  
This functional test assumes that [**By-IP-finder**](https://gitlab.com/NineKFlames/cml-test) app is running and accessible from `localhost`  

To launch it, execute [`maven`](https://maven.apache.org/download.cgi) in console from project dir:
```
mvn clean verify
```